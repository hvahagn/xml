﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Threading.Tasks;
// Выборка из XML с помощью XPath. (Запросы XPath)

namespace XML_XPath_11
{
    class Program
    {
        static void Main(string[] args)
        {// Создание XPath документа.
            var document = new XPathDocument("books.xml");
            XPathNavigator navigator = document.CreateNavigator();

            // Прямой запрос XPath.
            XPathNodeIterator iterator1 = navigator.Select("ListOfBooks/Book/Title/");
            while (iterator1.MoveNext())
            {
                Console.WriteLine(iterator1.Current);
            }
            Console.WriteLine(new string('*', 50));
            // Скомпилированный запрос XPath

            XPathExpression expr = navigator.Compile("ListOfBooks/Book[2]/Price");
            XPathNodeIterator iterator2 = navigator.Select(expr);
            while (iterator2.MoveNext())
            {
                Console.WriteLine(iterator2.Current);
            }

            Console.ReadLine();
        }
    }
}
