﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XML_08Writer
{
    class Program
    {
        static void Main(string[] args)
        {

            var xmlWriter = new XmlTextWriter("books.xml", null);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("ListOfBooks");
            xmlWriter.WriteStartElement("Book");
            xmlWriter.WriteString("Title-1");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Book");
            xmlWriter.WriteString("Title-2");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.Close();

            Console.WriteLine("books.xml created!");

            Console.ReadLine();
        }
    }
}
