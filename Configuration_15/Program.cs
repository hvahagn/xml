﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Specialized;

namespace Configuration_15
{
    class Program
    {
        public static object ConfigurationManager { get; private set; }

        static void Main(string[] args)
        {

            NameValueCollection allAppSettings = ConfigurationManager.AppSettings;
            Int32 counter = 0;
            IEnumerator settingEnumerator = allAppSettings.Keys.GetEnumerator();


            while (settingEnumerator.MoveNext())
            {
                Console.WriteLine("Item: {0} Value: {1}", allAppSettings.Keys[counter], allAppSettings[counter]);
                counter++;
            }

            // Delay.
            Console.ReadKey();
        }
    }
}
