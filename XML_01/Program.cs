﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
namespace XML_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var document = new XmlDocument();
            document.Load("books.xml");


            // Показ содержимого XML.
            Console.WriteLine(document.InnerText);
            Console.WriteLine(new string('*', 50));

            // Показ кода XML документа.
            Console.WriteLine(document.InnerXml);




            Console.ReadLine();
        }
    }
}
