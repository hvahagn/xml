﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace XML_TextReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.SetWindowSize(80, 45);
            FileStream stream = new FileStream("books.xml", FileMode.Open);
            XmlTextReader xmlReader = new XmlTextReader(stream);

            while (xmlReader.Read())
            {
                Console.WriteLine("{0,-10}{1,-10}{2, -10}",
                    xmlReader.NodeType, 
                    xmlReader.Name, 
                    xmlReader.Value);
            }
            xmlReader.Close();
            Console.ReadLine();
        }
    }
}
