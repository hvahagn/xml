﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XML_09Writer
{
    class Program
    {
        static void Main(string[] args)
        {
            var xmlWriter = new XmlTextWriter("books.xml", Encoding.GetEncoding(1251));
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("ListOfBooks");
            xmlWriter.WriteComment("This is comment");
            xmlWriter.WriteEndElement();

            xmlWriter.Close();

            Console.WriteLine("books.xml created!");

            Console.ReadLine();
        }
    }
}
